import unittest

from sb_fractalicious.raw_python.GridFractal import GridFractal
from test.raw_python.test_Fractal import TestFractal


class TestGridFractal(TestFractal):

    def setUp(self):
        self.fractal = GridFractal()

    @unittest.expectedFailure
    def test_equation(self):
        raise ValueError()

    @unittest.expectedFailure
    def test_generate_fractal(self):
        raise ValueError()
