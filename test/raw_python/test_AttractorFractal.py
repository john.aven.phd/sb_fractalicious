import unittest

from sb_fractalicious.raw_python.AttractorFractal import AttractorFractal
from test.raw_python.test_Fractal import TestFractal


class TestAttractorFractal(TestFractal):

    def setUp(self):
        self.fractal = AttractorFractal

    @unittest.expectedFailure
    def test_equation(self):
        raise ValueError()

    @unittest.expectedFailure
    def test_generate_fractal(self):
        raise ValueError()
