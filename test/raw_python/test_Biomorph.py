import unittest

from sb_fractalicious.raw_python.Biomorph import Biomorph
from test.raw_python.test_GridFractal import TestGridFractal


class TestBiomorph(TestGridFractal):

    def setUp(self):
        self.fractal = Biomorph(c=1 + 1j)

    def test_equation(self):
        c0 = 1
        c1 = 0.1
        c2 = 0.5 + .5j
        c3 = 1.1j
        self.assertEqual(30, self.fractal._equation(inputs=c0))
        self.assertEqual(22, self.fractal._equation(inputs=c1))
        self.assertEqual(4, self.fractal._equation(inputs=c2))
        self.assertEqual(5, self.fractal._equation(inputs=c3))

    @unittest.expectedFailure
    def test_generate_fractal(self):
        raise ValueError()
