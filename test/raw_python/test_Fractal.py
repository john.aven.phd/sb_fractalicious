import unittest
from unittest import TestCase

from sb_fractalicious.raw_python.Fractal import Fractal


class TestFractal(TestCase):

    def setUp(self):

        self.fractal = Fractal()

    @unittest.expectedFailure
    def test_equation(self):
        self.fractal.generate_fractal()

    @unittest.expectedFailure
    def test_generate_fractal(self):
        self.fractal.generate_fractal()
