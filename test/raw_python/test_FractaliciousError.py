from unittest import TestCase

from sb_fractalicious.raw_python.FractaliciousError import FractaliciousError


class TestFractaliciousError(TestCase):

    def test_FractaliciousError(self):

        with self.assertRaises(FractaliciousError):
            raise FractaliciousError('Somer Error')

        self.assertIsInstance(FractaliciousError('error'), FractaliciousError)

        self.assertIsInstance(FractaliciousError('error'), Exception)
