import os
from setuptools import setup


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...


def read(file_name):
    return open(os.path.join(os.path.dirname(__file__), file_name)).read()


setup(
    name="py_fractalicious",
    version="0.0.1",
    author="John Aven",
    author_email="dr.john.aven.phd@gmail.com",
    description= "Just a fun package for generating fractal images. Designed to be extensible. "
                 "Please enjoy and provide feedback!.",
    license="Apache version 2.0",
    keywords="fractal, mandelbrot, biomorph",
    url="https://gitlab.com/john.aven.phd/sb_fractalicious",
    packages=['sb_dev_tools', 'test'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 2 - Alpha",
        "Topic :: Scientific/Engineering :: Mathematics",
        "License :: OSI Approved :: Apache Software License  ",
    ], install_requires=['numpy', 'matplotlib']
)
