# sb_fractalicious

Fractals are cool, and fractals are fun. Mandelbrot is a god among geomtrists.

This fun little project is meant to help with computing fractals and generating 
amazing images of them.

## Example

Example of standard Mandelbrot set

```python

    # Create the class instance
    m = Mandelbrot(order=2,
                   x_resolution=2000,
                   y_resolution=2000,
                   x_range=(-1, 1),
                   y_range=(-1, 1))


    # plot the computed Mandelbrot set
    m.run(filename='my_fract.png',
          resoltuion=2000)

 ```
## Sample Images

 **Mandelbrot**

 ![alt text](img/mandelbrot.png "mandelbrot")

 **Biomorph**

![alt text](img/biomorph.png "bbiomorph")