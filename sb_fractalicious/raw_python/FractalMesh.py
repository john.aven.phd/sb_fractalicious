import numpy as np


class FractalMesh:

    def __init__(self, **kwargs):

        if 'x_range' in kwargs.keys():
            self._x_range = kwargs.pop('x_range')
        else:
            self._x_range = (-2.0, 0.5)

        if 'y_range' in kwargs.keys():
            self._y_range = kwargs.pop('y_range')
        else:
            self._y_range = (-1.1, 1.0)

        if 'x_resolution' in kwargs.keys():
            self._x_resolution = kwargs.pop('x_resolution')
        else:
            self._x_resolution = 2000

        if 'y_resolution' in kwargs.keys():
            self._y_resolution = kwargs.pop('y_resolution')
        else:
            self._y_resolution = 2000

        # Create/build the mess array
        self.array = self._build_mesh()

    def _build_mesh(self) -> np.ndarray:
        """
        Construct the meshgrid for the fractal

        Returns: numpy ndarray of complex values for the mesh.

        """

        x = np.linspace(self._x_range[0],
                        self._x_range[1],
                        self._x_resolution)

        y = np.linspace(self._y_range[0],
                        self._y_range[1],
                        self._y_resolution)

        # Create the mesh grid to compute over
        xx, yy = np.meshgrid(x, y)

        # mesh grid is converted to complex space to make computations easier.
        return xx + 1j * yy
