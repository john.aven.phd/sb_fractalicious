import numpy as np

from sb_fractalicious.raw_python.GridFractal import GridFractal


class Biomorph(GridFractal):

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        # Get the set order from input if provided
        if 'order' in kwargs.keys():
            self._order = kwargs.pop('order')
        else:
            self._order = 2

        if 'c' in kwargs.keys():
            self._constant = kwargs.pop('c')
        else:
            self._constant = complex(.511, .5)

    def _equation(self, inputs: list) -> object:
        """
        Biomorph based fractals:
        z_n+1 = sin(z_n) + exp(z) + c
        where z,c are complex and the solutions are over z.

        This is iterated with fixed c and initial value of c. When z > 4 the value is set to the constant of
        that iteration.

        When the number of iterations has reached or passed the max allowable number of iterations, then the value
        returned is the max allowed number of iterations.

        Args:
            inputs: the inputs are a scalar and are z.

        Returns:
            The number of iterations it took for the amplitude of the point to exceed 4

        """

        zn = 0

        current_iteration = 0

        z = inputs

        while current_iteration < self._max_iter:

            zn = np.sin(zn) + np.exp(z) + self._constant
            current_iteration = current_iteration + 1

            if zn.real**2 + zn.imag**2 > 30:
                return current_iteration

        return self._max_iter


if __name__ == '__main__':

    # Create the class instance
    m = Biomorph(x_resolution=2000,
                 y_resolution=2000,
                 x_range=(-2.5, 2.5),
                 y_range=(3.3, 10))

    # plot the computed Mandelbrot set
    m.generate_fractal(filename='biomorph.pdf',
                       resolution=500)
