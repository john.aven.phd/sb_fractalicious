import numpy as np
import matplotlib.pyplot as plt

from sb_fractalicious.raw_python.Fractal import Fractal
from sb_fractalicious.raw_python.FractalMesh import FractalMesh


class GridFractal(Fractal):

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        # This is the max iteration any one point wil take. Too many will take too long to compute. Perhaps on
        # refinement we can allow for a larger number
        if 'max_iter' in kwargs.keys():
            self._max_iter = kwargs.pop('max_iter')
        else:
            self._max_iter = 30

        self.mesh = FractalMesh(**kwargs)

    def generate_fractal(self, filename='default.png', resolution=500):

        result = np.vectorize(self._equation)(self.mesh.array)

        plt.imshow(result)

        fig = plt.gcf()
        fig.set_size_inches((8.5, 11), forward=False)
        fig.savefig(filename, dpi=resolution)

        return None
