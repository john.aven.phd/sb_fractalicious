from sb_fractalicious.raw_python.Fractal import Fractal


class AttractorFractal(Fractal):
    """
    Base class for fractal attractors.
    """

    def generate_fractal(self, filename='default.png', resolution=500) -> None:

        raise NotImplementedError('Base class method. Please implement.')
