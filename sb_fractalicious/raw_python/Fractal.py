import numpy as np
from matplotlib import pyplot as plt


class Fractal:

    def __init__(self, **kwargs):
        pass

    def _equation(self, inputs: list) -> object:

        raise ValueError('Base class method. Please write implementation')

    def generate_fractal(self, filename='default.png', resolution=500) -> None:
        """
        This executes the creation of the mandelbrot set and prints it.

        Returns: None

        """

        raise NotImplementedError('Base class method. Please write implementation')
