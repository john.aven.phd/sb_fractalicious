import numpy as np

from sb_fractalicious.raw_python.Fractal import Fractal


class Biomorphs(Fractal):

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        self._max_iter = 40
        self._constant = complex(-.5, .5)

    def _equation(self, inputs: list) -> object:

        # f(zn) = sin(zn) + ez + c
        z = 0

        current_iteration = 0

        z = inputs

        while current_iteration < self._max_iter:

            z = np.sin(z) + np.exp(z) + self._constant
            current_iteration = current_iteration + 1

            if z.real**2 + z.imag**2 > 4:
                return current_iteration

        if isinstance(z, complex):
            return self._max_iter


if __name__ == '__main__':

    # Create the class instance
    m = Biomorphs(x_resolution=5000,
                  y_resolution=5000,
                  x_range=(-10, 10),
                  y_range=(-10, 10))

    # plot the computed Mandelbrot set
    m.generate_fractal(filename='biomorph.pdf',
                       resolution=500)
