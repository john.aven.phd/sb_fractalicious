from sb_fractalicious.raw_python.GridFractal import GridFractal


class Mandelbrot(GridFractal):
    """
    Computes the Mandelbrot set.
    """

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        # Get the set order from input if provided
        if 'order' in kwargs.keys():
            self._order = kwargs.pop('order')
        else:
            self._order = 2

    def _equation(self, inputs: list) -> object:
        """
        This is the main equation of the whole thing.

        z = z^2 + c

        This is iterated with fixed c and initial value of c. When z > 4 the value is set to the constant of
        that iteration.

        When the number of iterations has reached or passed the max allowable number of iterations, then the value
        returned is the max allowed number of iterations.

        Args:
            inputs: constant value in the equation (c)

        Returns:
            The number of iterations it took for the amplitude of the point to exceed 4

        """

        z = 0

        current_iteration = 0

        c = inputs

        while current_iteration < self._max_iter:

            z = z ** self._order + c
            current_iteration = current_iteration + 1

            if z.real**2 + z.imag**2 > 4:
                return current_iteration

        return self._max_iter
        # if isinstance(z, complex):
        #     return self._max_iter


if __name__ == '__main__':

    # Create the class instance
    m = Mandelbrot(order=9,
                   x_resolution=500,
                   y_resolution=500,
                   x_range=(-1, 1),
                   y_range=(-1, 1))

    # plot the computed Mandelbrot set
    m.generate_fractal(filename='mandelbrot.png',
                       resolution=2000)
